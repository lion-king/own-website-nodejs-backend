const express = require("express");
const route_config = require('./config/routepath')
const DbConnection = require('./connection/connectionDB')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
var cookieParser = require('cookie-parser')
var fs = require('fs')
var helmet = require('helmet')
const compression = require('compression');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
require('dotenv').config()
const app = express();
const port = process.env.SERVER_PORT || 4000;

var accessLogStream = fs.createWriteStream('./access.log',{flags:'a'})
app.use(bodyParser.urlencoded({extented:false}));
app.use(bodyParser.json())
app.use(compression())
app.use(cors())
app.use(cookieParser())
app.use(morgan('combined',{stream:accessLogStream}))
// Don't expose any software information to potential hackers.
app.disable('x-powered-by');
app.use(helmet())
app.get('/',(req,res) => {
    res.status(200).send("welcome to admin panel network");
    res.end();
})

// Admin APIs routes
app.use('/api',require(`${route_config.admin_route_path}`))

// 404 Error
app.get('*',(req,res)=>{res.status(404).send('404 Not Found');});



if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);
  
    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
      cluster.fork();
    }
  
    cluster.on('exit', (worker, code, signal) => {
      console.log(`worker ${worker.process.pid} died`);
    });
  } else {
    // Workers can share any TCP connection
    // In this case it is an HTTP server
    const Server = app.listen(port,() => {console.log(`Server started on port ${process.env.SERVER_PORT}`)});

    console.log(`Worker ${process.pid} started`);
  }  

