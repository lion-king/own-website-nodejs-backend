const mongoose = require('mongoose')
const loginUser = require('../models/loginUser')
const connect = require('../connection/connectionDB')
const User = mongoose.model('user', loginUser)

    async function  createUser(username,password) {
        return new User({
          username,
          password,
          created: Date.now()
        }).save()
      }
      
      async function  findUser(username) {
        return await User.findOne({ username })
      }
       async function data() {
        const connector = connect;
      console.log(connector)
        // let user = await connector.then(async () => {
        //   return findUser("rishabh")
        // })
        let user =  findUser("rishabh")
        if (!user) {
          user = await createUser("rishabh","rishabh")
        }
        console.log(user)
      }
      

module.exports.loginService =data;