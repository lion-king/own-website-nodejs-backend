module.exports = {
    errorResponseHandler:function(res, responseObject, message = '', status = 400) {
        return res.status(status).json({
            'error':true,
            'message':message,
            'response':responseObject
        });
    },
    successResponseHandler:function(res, responseObject, message = '', status = 200) {
        return res.status(status).json({
            'error':false,
            'message':message,
            'response':responseObject
        });
    },
}