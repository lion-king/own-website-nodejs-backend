const router = require('express').Router();
const adminPath = require('../controllers/AdminController');
router.get('/home',adminPath.home);

module.exports = router;